import kotlin.properties.Delegates

fun main(args: Array<String>) {
    val person = Person()

    person.observer = LogPersonMoodObserver()
    person.mood = "Emocionado"
    person.mood = "Impavido"
    person.mood = "Impactado"
}

interface MoodsObserver{
    fun onMoodChange(newMood: String)
}

class LogPersonMoodObserver: MoodsObserver{
    override fun onMoodChange(newMood: String) {
        println("La persona cambio de estado a:  $newMood")
    }
}

class Person {
    var observer: MoodsObserver? = null

    var mood: String by Delegates.observable(""){
            property, oldValue, newValue ->  observer?.onMoodChange(newValue)
    }
}